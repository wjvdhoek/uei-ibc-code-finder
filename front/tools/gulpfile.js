"use strict";

var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var transform = require('vinyl-transform');
var source = require('vinyl-source-stream');
var include = require('gulp-include');
var notify = function() { return { on: function(a) { return a; }, emit: function(b) { return b; }, once: function(c) { return c; }, end: function(d) { return d; }, removeListener: function(e) { return e; }, write: function(f) { return f; } } };
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sass = require('gulp-ruby-sass');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var destination = '../../public/';

var setDestination = function(loc) {
    destination = loc;
};

gulp.task('htmlbuilder', function() {
	gulp.src('../src/html/*.html')
		.pipe(gulp.dest(destination))
		.pipe(notify({message: 'Copy html'}));
});

gulp.task('imgbuilder', function() {
	gulp.src('../src/images/*.*')
		.pipe(gulp.dest(destination + 'images/'))
		.pipe(notify({message: 'Copy images'}));
});

gulp.task('jsbuilder', function() {
	return browserify('../src/js/scripts.js')
        .transform(babelify)
        .bundle()
    	//.pipe(uglify())
    	.pipe(source('scripts.js'))
    	.pipe(gulp.dest(destination + 'js/'))
		.pipe(notify({ message: 'Compiled js'}));
});

gulp.task('jsvendorbuilder', function() {
	//'../src/vendor/PE-Framework.js',
	gulp.src([
    '../src/vendor/jquery-1.11.3.min.js',
    '../src/vendor/jquery-ui.min.js',
    '../src/vendor/jquery.transit.min.js',
    '../src/vendor/PE-Framework.js'
    ])
		.pipe(concat('vendor-scripts.js'))
		.pipe(uglify({mangle: false}))
		.pipe(gulp.dest(destination + 'js/'))
		.pipe(notify({ message: 'Compiled js'}));
});

gulp.task('jswatch', function() {
	gulp.watch(['../src/js/**/*.js', '../src/js/*.js'], ['jsbuilder']);
});


gulp.task('cssbuilder', function() {
	/* {style: 'compressed', 'sourcemap=none': true } */
	sass('../src/sass/styles.scss')
		.pipe(rename('styles.css'))
		.pipe(autoprefixer({browsers: ['last 2 versions']}))
		/* .pipe(minifyCss({compatibility: 'ie8'})) */
		.pipe(gulp.dest(destination + 'css/'))
		.pipe(notify({ message: 'Compiled sass'}));
});

gulp.task('csswatch', function() {
	gulp.watch(['../src/sass/*.scss'], ['cssbuilder']);
});

gulp.task('htmlwatch', function() {
	gulp.watch(['../src/html/*.html'], ['htmlbuilder']);
});

gulp.task('imgwatch', function() {
	gulp.watch(['../src/images/*.*'], ['imgbuilder']);
});

gulp.task('default', function() {
	gulp.start('htmlbuilder');
	gulp.start('imgbuilder');
	gulp.start('jsbuilder');
	gulp.start('jsvendorbuilder');
	gulp.start('cssbuilder');
	//gulp.start('cssvendorbuilder');
	gulp.start('csswatch');
	gulp.start('jswatch');
	gulp.start('htmlwatch');
	gulp.start('imgwatch');
});

gulp.task('release', function(){
    gulp.start('htmlbuilder');
    gulp.start('imgbuilder');
    gulp.start('jsbuilder');
    gulp.start('jsvendorbuilder');
    gulp.start('cssbuilder');
});
