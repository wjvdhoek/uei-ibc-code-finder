"use strict";

// _PE.modules.autoComplete = {
// 	id: 'autoComplete',
// 	PEClass: '#brand',
//
// 	init: function() {
//     var availableBrands = [
//       "Samsung",
//       "Sony",
//       "Other..."
//     ];
//     this.elements.autocomplete({
//       source: availableBrands
//     });
// 	}
// };


_PE.modules.select = {
	id: 'select',
	PEClass: '.pe_select',
	selects: [],
	hiddenOptions: [],

	init: function() {
			var self = this;

			this.elements.each(function(idx, select) {
				self.createDOM($(select));
				self.setValueFeedback($(select).data('rel'), $(select).find('option[value="' + $(select).val() + '"]').text());
			});

			this.selects = $(this.selects);
			this.bindEvents();

			$('.grouped-radio input:checked').trigger('change');

			$('span.val').text(this.elements.data('placeholder'));
	},

	createDOM: function(select) {

		var self = this;

		select.data('options', select.find('>option, >optgroup'));

		// Wrap the select element in a new elements

		var wrap = $('<div class="select"><span class="val"></span><span class="toggle"></span><div class="list"><span class="text-input"><input type="text" id="searchValue" /></span></div></div>');
		select.after(wrap);
		wrap.append(select);
		wrap.addClass(select[0].className);
		wrap.data('rel', select);
		select.data('rel', wrap);

		var list = wrap.find('.list');

		select.data('options').each(function(idx, opt) {

			if ($(opt).is('optgroup')) {

				if(select.hasClass('pe_group-is-clickable')) {

					// Add option to the original selectbox
					select.append('<option value="' + $(opt).attr('value') + '">' + $(opt).attr('label') + '</option>');

					// Groups are clickable
					var group = $('<span class="option"></span>'),
						options = $(opt).find('option');

					group.text($(opt).attr('label'));
					group.data('rel', $(opt));
					$(opt).data('rel', group);
					list.append(group);

				} else {

					// Groups not clickable
					var group = $('<span class="group"></span>'),
						options = $(opt).find('option');

					group.text($(opt).attr('label'));
					group.data('rel', $(opt));
					$(opt).data('rel', group);
					list.append(group);
				}

				options.each(function(idx, opt) {
					var option = $('<span class="option child"></span>');
					option.text($(opt).text());
					option.data('rel', $(opt));
					$(opt).data('rel', option);
					list.append(option);
				});

			} else {
				var option = $('<span class="option"></span>');
				option.text($(opt).text());
				option.data('rel', $(opt));
				$(opt).data('rel', option);
				list.append(option);
			}

		});

		var options = list.find('.option');
		options.filter(':first').addClass('first');
		options.filter(':last').addClass('last');

		this.selects.push(wrap[0]);
	},

	setValueFeedback: function(select, value) {
		select.find('.val').text(value);
	},

	bindEvents: function() {
		var self = this;

		$(document).on('click', function(e) {
			var src = $(e.currentTarget),
				trueSrc = $(e.target);

			if (!trueSrc.parents('.select').length) self.selects.removeClass('open');
		});

		$('input[name="device"]').on('change', function(e) {
			self.hiddenOptions = [];
			switch ($(this).val()) {
				case 'tv':
					self.selects.find('.option').each(function() {
						if (['Samsung', 'Sony','Other'].indexOf($(this).text()) < 0) {
							console.log($(this));
							$(this).hide();
							self.hiddenOptions.push($(this).text());
						}
					});
					break;
				case 'audio':
					self.selects.find('.option').each(function() {
					if (['Onkyo', 'Sony','Other'].indexOf($(this).text()) < 0) {
						console.log($(this));
						$(this).hide();
						self.hiddenOptions.push($(this).text());
					}
				});
				break;
			}
		});

		this.elements.on('change', function(e) {
			var src = $(e.currentTarget),
				trueSrc = $(e.target);

			self.setValueFeedback(src.data('rel'), src.find('option[value="'+src.val()+'"]').text());

		});

		this.selects.on('click', function(e) {
			var src = $(e.currentTarget),
				trueSrc = $(e.target);

			if (trueSrc.is('.option')) {
				var oldVal = src.data('rel').val(),
					newVal = trueSrc.data('rel').attr('value');

				src.data('rel').val(trueSrc.data('rel').attr('value'));
				self.setValueFeedback(src, trueSrc.data('rel').text());
				src.removeClass('open');
				if (newVal != oldVal) src.data('rel').trigger('change');
				e.preventDefault();
			} else if (trueSrc.parent().is('.option')) {
				var oldVal = src.data('rel').val(),
					newVal = trueSrc.parent().data('rel').attr('value');

				src.data('rel').val(trueSrc.parent().data('rel').attr('value'));
				self.setValueFeedback(src, trueSrc.parent().data('rel').text());
				src.removeClass('open');
				if (newVal != oldVal) src.find('select').trigger('change');
				e.preventDefault();
			} else {
				if (!src.is('.open')) {
					self.selects.removeClass('open');
					src.addClass('open');
					src.find('.option').each(function() {
						if (!$(this).is(':visible') && self.hiddenOptions.indexOf($(this).text()) < 0) {
							$(this).show();
						}
					});
				} else {
					src.removeClass('open');
				}
			}

			src.find('input[type="text"]').focus();
		});

		$('#searchValue').on('keyup', function(e) {
			var src = $(this),
					val = src.val().toLowerCase(),
					options = src.parent().siblings('.option');

			options.each(function() {
				var src = $(this),
						text = src.text().toLowerCase();

				if (text.indexOf(val) < 0) {
					src.hide();
				}
				if (text.indexOf(val) > -1 && !src.is(':visible') && self.hiddenOptions.indexOf(src.text()) < 0) {
					src.show();
				}
			});
		});
	}
};

_PE.modules.groupedRadio = {
	id: "groupedRadio",
	PEClass: '.grouped-radio',

	init: function() {
		this.bindEvents();
	},

	bindEvents: function() {
		var self = this;
		this.elements.find('label').each(function() {
			var $this = $(this);
			$this.on('click', function(e) {
				var $this = $(this);
				if ($this.attr('for') == $(self.elements.find('.checked')).attr('for')) {
					return;
				}
				self.elements.find('label').toggleClass('checked');
				self.elements.find('input[type="radio"]:checked').prop('checked', false);
				$this.find('input[type="radio"]').prop('checked', true).trigger('change');
				e.preventDefault();
			});
		});
	}
}

_PE.modules.showCode = {
	id: 'showCode',
	PEClass: '.code-container',

	codes: {
		samsungTv: '2051',
		sonyTv: '1825',
		otherTv: '2118',
		sonyAudio: '1758',
		onkyoAudio: '1805',
		otherAudio: '8287'
	},

	device: 'tv',
	brand: null,
	code: null,

	init: function() {
		this.brand = $('#brand').val();
		this.bindEvents();
	},

	bindEvents: function() {
		var self = this,
				device = $('input[name="device"]'),
				brand = $('#brand'),
				btn = $('#showCode'),
				form = $('#codeFinderForm');

		device.on('change', function(e) {
			var $this = $(this);
			self.device = $this.val();
			// e.preventDefault();
		});

		brand.on('change', function(e) {
			var $this = $(this);
			self.brand = $this.val().toLowerCase();
			// e.preventDefault();
		});

		btn.on('click', function(e) {
			self.setCode();
			e.preventDefault();
		});

		form.on('submit', function(e){
			e.preventDefault();
			return false;
		});
	},

	setCode: function() {
		if (this.brand === null || this.device === null) {
			return;
		}

		var code = null;

		switch(true) {
			case this.device == 'tv' && this.brand == 'samsung':
				code = this.codes.samsungTv;
				break;

			case this.device == 'tv' && this.brand == 'sony':
				code = this.codes.sonyTv;
				break;

			case this.device == 'tv':
				code = this.codes.otherTv;
				break;

			case this.device == 'audio' && this.brand == 'sony':
				code = this.codes.sonyAudio;
				break;

			case this.device == 'audio' && this.brand == 'onkyo':
				code = this.codes.onkyoAudio;
				break;

			case this.device == 'audio':
				code = this.codes.otherAudio;
				break;

			default:
				this.elements.hide();
				return false;
				break;
		}

		if (!this.elements.is(":visible")) {
			this.elements.show();
		}

		this.elements.find('#number_one').text(code[0]);
		this.elements.find('#number_two').text(code[1]);
		this.elements.find('#number_three').text(code[2]);
		this.elements.find('#number_four').text(code[3]);

		if (this.device == 'tv') {
			$('#btnClass').removeClass('blue')
		} else {
			$('#btnClass').addClass('blue')
		}
	}
}
